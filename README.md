# workshop-website

A website for our workshop.

> https://zine-machines.kstraat.casa

## Hack On It

Just get ripping on the [index.html].

[index.html]: https://git.vvvvvvaria.org/zine-machines/workshop-website/src/branch/master/index.html

## Deploy It

Firstly, send [@lwm] an SSH public key part ([read here how to make one]).

[@lwm]: https://git.vvvvvvaria.org/lwm
[read here how to make one]: https://help.github.com/articles/generating-a-new-ssh-key-and-adding-it-to-the-ssh-agent/#generating-a-new-ssh-key

Then, once your key has been added to the server (I let you know), you can do:

```
$ git remote add dokku ssh://dokku@zine-machines.kstraat.casa:22/zine-machines
$ git push dokku master
```

Watch the deployment run and then check the new site at:

> https://zine-machines.kstraat.casa
